package zhengguofeng.game;


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;


public class MyGameFream extends JFrame{
    Image airpalan = GameUtil.getImage("tupian/plane.png");
    Image bacoground = GameUtil.getImage("tupian/bg.jpg");
    Plane plane =new Plane(airpalan,400,400);
    Shell shell[] =new Shell[20];
    Explode bao;
    Date startTime = new Date();
    Date endTime;
    int period; //  游戏持续时间；


    @Override
    public void paint(Graphics g) {

        g.drawImage(bacoground,0,0,null);
        plane.drawSelf(g);    // 飞机
        for(int i=0;i<shell.length;i++){
           shell[i].draw(g);
           boolean peng= shell[i].getRect().intersects(plane.getRect());
           if(peng){
               plane.live=false;
               if(bao==null){
                   bao= new Explode(plane.x,plane.y);
                   endTime= new Date();

                   period = (int) (endTime.getTime()-startTime.getTime())/1000;
               }
               bao.draw(g);
           }
           if(!plane.live) {
               Font f = new Font("宋体",Font.BOLD,30);
               g.setFont(f);
               g.drawString("时间:" + period + "秒", (int) plane.x, (int) plane.y);
           }
        }

    }


    class PaintThread extends  Thread{
        @Override
        public void run(){
            while (true){
                repaint();  //重画  画面
                try {
                    Thread.sleep(40);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
class KeyMonitor extends KeyAdapter{
        //按下键  行动
    @Override
    public void keyPressed(KeyEvent e) {
        plane.addDirection(e);
    }
//键盘监听
    @Override
    //松开
    public void keyReleased(KeyEvent e) {
       plane.minusDirection(e);
    }
}

     /*
     * 初始化窗口*/
    public void  lanuchFream(){
        this.setTitle("打飞机游戏");
        this.setVisible(true);
        this.setSize(Constant.GAME_WIDTH,Constant.GAME_HEIGHT);
        this.setLocation(300,300);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0 );
            }
        });
        new PaintThread().start(); //启动窗口的线程
        addKeyListener(new KeyMonitor());
        //初始化炮弹
        for(int i=0;i<shell.length;i++){
            shell[i]=new Shell();
        }
    }

    public static void main(String[] args) {
             MyGameFream f= new MyGameFream();
             f.lanuchFream();
    }
    private Image offScreenImage = null;

    public void update(Graphics g) {
        if(offScreenImage == null)
            offScreenImage = this.createImage(500,500);//这是游戏窗口的宽度和高度

        Graphics gOff = offScreenImage.getGraphics();
        paint(gOff);
        g.drawImage(offScreenImage, 0, 0, null);
    }
}
